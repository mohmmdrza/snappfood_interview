import React from 'react';
import { Provider } from 'react-redux';
import { Route, BrowserRouter, Routes } from 'react-router-dom';
import Main from './components/main/Main';
import { store } from './redux/store';

function App() {
  return (
    <Provider store={store}>
      <Routers/>
    </Provider>
  );
}

function Routers() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Main/>}/>
      </Routes>
    </BrowserRouter>
  )
}

export default App;
