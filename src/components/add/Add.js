import React, { useState } from 'react';
import { connect } from 'react-redux';
import Check from '../../assets/icons/check.png';
import { addTodo, setItemsRemained } from '../../redux/actions';
import './Add.scss';

function Add({ ...props }) {
    const [newTodo, setNewTodo] = useState('');

    const handleAdd = () => {
        props.addTodo(
            {
                name: newTodo,
                finished: false
            }
        );
        props.setItemsRemained(1);
        setNewTodo('');
    }

    return (
        <div className={'addContainer'}>
            <input
            placeholder={'What need to be done?'}
            value={newTodo}
            onChange={(e) => setNewTodo(e.target.value)}
            />
            {newTodo !== '' ?
                <img src={Check} alt={'check'} onClick={handleAdd}/>
            : null}
        </div>
    )
}

export default connect(null, { addTodo, setItemsRemained })(Add);