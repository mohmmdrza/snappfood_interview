import React from 'react';
import { connect } from 'react-redux';
import { useLocation } from 'react-router-dom';
import Add from '../add/Add';
import Details from '../details/Details';
import Items from '../items/Items';
import './Main.scss';

function Main({ ...props }) {
    const filter = useLocation().search;
    const todoFilter = new URLSearchParams(filter).get('filter');
    return (
        <div className={'mainContainer'}>
            <p className={'title'}>
                Todo
            </p>
            <div className={'todoBox'}>
                <Add/>
                {props.todoList.map((item, index) => (
                    parseInt(todoFilter) !== 2 ?
                        <Items 
                        data={item} 
                        index={index}
                        key={item.name}/> 
                    :
                        item.finished === false ?
                            <Items 
                            data={item} 
                            index={index}
                            key={item.name}/> 
                        : null
                ))}
            </div>
            <Details 
            itemsRemained={props.itemsRemained}
            filter={todoFilter}
            />
        </div>
    )
}

const mapStateToProps = (state) => ({
    todoList: state.todoList,
    itemsRemained: state.itemsRemained
})

export default connect(mapStateToProps, null)(Main);