import React, { useState } from 'react';
import Close from '../../assets/icons/close.png';
import Check from '../../assets/icons/check.png';
import './Items.scss';
import { deleteTodo, editTodo, setItemsRemained } from '../../redux/actions';
import { connect } from 'react-redux';

function Items({ data, index, ...props }) {
    const [item, setItem] = useState(data);
    const [editName, setEditName] = useState(false);

    //delete an item
    const handleDeleteItem = () => {
        props.deleteTodo(index);
        if(data.finished === false) {
            props.setItemsRemained(-1);
        }
    }

    //change an item situation
    const handleFinishedTodo = () => {
        props.editTodo(index, item.name, true);
        setItem({
            'name': item.name,
            'finished': true
        })
        props.setItemsRemained(-1);
    }

    //handle double click and open edit name view
    const handleOpenEdit = (e) => {
        switch (e.detail) {
            case 2:
                setEditName(true)
            break;
            default:
                return;
        }
    };

    //close edit name view
    const handleCloseEdit = () => {
        setItem(data)
        setEditName(false);
    }
    
    //change item name locally
    const editItemName = (e) => {
        setItem({
            'name': e.target.value,
            'finished': item.finished
        })
    }

    //change item name in todo list
    const handleEditName = () => {
        props.editTodo(index, item.name, item.finished);
        setEditName(false);
    }

    return (
        <div className={'itemContainer'}>
            {editName ? 
                <>
                    <input
                    value={item.name}
                    onChange={(e) => editItemName(e)}
                    />
                    <div className={'actionsContainer'}>
                        <img 
                        onClick={handleEditName} 
                        src={Check} 
                        alt={'check'}
                        className={'checkImg'}/>
                        <img 
                        src={Close} 
                        alt={'close'} 
                        onClick={handleCloseEdit}/>
                    </div>
                </>
            : 
                <>
                    <p onClick={handleOpenEdit}>
                        {item.name}
                    </p>
                    <div className={'actionsContainer'}>
                        {!item.finished &&
                            <img 
                            onClick={handleFinishedTodo} 
                            src={Check} 
                            alt={'check'}
                            className={'checkImg'}/>
                        }
                        <img 
                        src={Close} 
                        alt={'close'} 
                        onClick={handleDeleteItem}/>
                    </div>
                </>
            }
        </div>
    )
}

export default connect(null, { deleteTodo, setItemsRemained, editTodo })(Items);