import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteCompleted } from '../../redux/actions';
import './Details.scss';

function Details({ itemsRemained, filter, ...props }) {

    return (
        <div className={'detailsContainer'}>
            <p className={'itemsRemained'}>
                <span>{itemsRemained}</span> items left
            </p>
            <div className={'filter'}>
                <Link to={'?filter=1'}>
                    <p className={parseInt(filter) !== 2 ? 'active' : ''}>
                        All
                    </p>
                </Link>
                <Link to={'?filter=2'}>
                    <p className={parseInt(filter) === 2 ? 'active' : ''}>
                        Active
                    </p>
                </Link>
            </div>
            <button
            onClick={props.deleteCompleted}
            className={'clearCompleted'}>
                Clear completed
            </button>
        </div>
    )
}

export default connect(null, { deleteCompleted })(Details);