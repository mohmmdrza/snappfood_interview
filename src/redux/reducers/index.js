import { PURGE } from 'redux-persist';
import { ADD_TODO, SET_TODO, SET_REMAINED } from '../actions/types';

const TODO_STATE = {
    todoList: [],
    itemsRemained: 0,
}

export default (state = TODO_STATE, action) => {
    switch(action.type) {
        case ADD_TODO:
            return {
                ...state,
                todoList: [...state.todoList, action.payload]
            }
        case SET_REMAINED:
            return {
                ...state,
                itemsRemained: state.itemsRemained + action.payload
            }
        case SET_TODO: 
            return {
                ...state,
                todoList: action.payload,
            }
        case PURGE:
                return {}
        default:
            return state
    }
}