import { ADD_TODO, SET_TODO, SET_REMAINED } from './types';

export const addTodo = (data) => async (dispatch) => {
    dispatch({
        type: ADD_TODO,
        payload: data
    })
}

export const setItemsRemained = (data) => async (dispatch) => {
    dispatch({
        type: SET_REMAINED,
        payload: data
    })
}

export const deleteTodo = (index) => async (dispatch, getState) => {
    const todoList = getState().todoList;
    let locTodoList = [...todoList];
    locTodoList.splice(index, 1);
    dispatch({
        type: SET_TODO,
        payload: locTodoList
    })
}

export const editTodo = (index, name, finished) => async (dispatch, getState) => {
    const todoList = getState().todoList;
    let locTodoList = [...todoList];
    locTodoList[index].name = name;
    locTodoList[index].finished = finished;
    dispatch({
        type: SET_TODO,
        payload: locTodoList
    })
}

export const deleteCompleted = () => async (dispatch, getState) => {
    const todoList = getState().todoList;
    let locTodoList = [...todoList];
    for (let i = locTodoList.length - 1; i >= 0; i--) {
        if (locTodoList[i].finished === true) { 
            locTodoList.splice(i, 1);
        }
    }
    dispatch({
        type: SET_TODO,
        payload: locTodoList
    })
}